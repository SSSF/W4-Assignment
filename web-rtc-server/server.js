const express = require('express');
const https = require('https');
const fs = require('fs');
const config = require('getconfig');
const sockets = require('signal-master/sockets');

const sslkey = fs.readFileSync('ssl-key.pem');
const sslcert = fs.readFileSync('ssl-cert.pem');

const options = {
      key: sslkey,
      cert: sslcert,
};

const app = express();

// Signalling server
const server = https.createServer(options, app).listen(8888);
sockets(server, config);
console.log('Signalling server listening on port 8888');
