// grab the room from the URL
const room = window.location.search && window.location.search.split('?')[1];

// create our webrtc connection
const webRTC = new SimpleWebRTC({
    // the id/element dom element that will hold "our" video
    localVideoEl: 'localVideo',
    // the id/element dom element that will hold remote videos
    remoteVideosEl: '',
    // immediately ask for camera access
    autoRequestMedia: true,
    debug: false,
    detectSpeakingEvents: true,
    autoAdjustMic: false,
    url: 'https://localhost:8888', // By default, SimpleWebRTC uses Google's public STUN server (stun.l.google.com:19302)
});

// when it's ready, join if we got a room from the URL
webRTC.on('readyToCall', () => {
    // you can name it anything
    if (room) webRTC.joinRoom(room);
});

const showVolume = (el, volume) => {
    if (!el) return;
    if (volume < -45) volume = -45; // -45 to -20 is
    if (volume > -20) volume = -20; // a good range
    el.value = volume;
}

// we got access to the camera
webRTC.on('localStream', (stream) => {
    const button = document.querySelector('form>button');
    if (button) button.removeAttribute('disabled');
    $('#localVolume').show();
});
// we did not get access to the camera
webRTC.on('localMediaError', (err) => {
    console.log('No access to the camera');
});

// local screen obtained
webRTC.on('localScreenAdded', (video) => {
    video.onclick = () => {
        video.style.width = video.videoWidth + 'px';
        video.style.height = video.videoHeight + 'px';
    };
    document.getElementById('localScreenContainer').appendChild(video);
    $('#localScreenContainer').show();
});
// local screen removed
webRTC.on('localScreenRemoved', (video) => {
    document.getElementById('localScreenContainer').removeChild(video);
    $('#localScreenContainer').hide();
});

// a peer video has been added
webRTC.on('videoAdded', (video, peer) => {
    console.log('video added', peer);
    const remotes = document.getElementById('remotes');
    if (remotes) {
        const container = document.createElement('div');
        container.className = 'videoContainer';
        container.id = 'container_' + webRTC.getDomId(peer);
        container.appendChild(video);

        // suppress contextmenu
        video.oncontextmenu = () => {
            return false;
        };

        // resize the video on click
        video.onclick = () => {
            container.style.width = video.videoWidth + 'px';
            container.style.height = video.videoHeight + 'px';
        };

        // show the remote volume
        const vol = document.createElement('meter');
        vol.id = 'volume_' + peer.id;
        vol.className = 'volume';
        vol.min = -45;
        vol.max = -20;
        vol.low = -40;
        vol.high = -25;
        container.appendChild(vol);

        // show the ice connection state
        if (peer && peer.pc) {
            const connstate = document.createElement('div');
            connstate.className = 'connectionstate';
            container.appendChild(connstate);
            peer.pc.on('iceConnectionStateChange', (event) => {
                switch (peer.pc.iceConnectionState) {
                case 'checking':
                    connstate.innerText = 'Connecting to peer...';
                    break;
                case 'connected':
                case 'completed': // on caller side
                    $(vol).show();
                    connstate.innerText = 'Connection established.';
                    break;
                case 'disconnected':
                    connstate.innerText = 'Disconnected.';
                    break;
                case 'failed':
                    connstate.innerText = 'Connection failed.';
                    break;
                case 'closed':
                    connstate.innerText = 'Connection closed.';
                    break;
                }
            });
        }
        remotes.appendChild(container);
    }
});
// a peer was removed
webRTC.on('videoRemoved', (video, peer) => {
    console.log('video removed ', peer);
    const remotes = document.getElementById('remotes');
    const el = document.getElementById(peer ? 'container_' + webRTC.getDomId(peer) : 'localScreenContainer');
    if (remotes && el) {
        remotes.removeChild(el);
    }
});

// local volume has changed
webRTC.on('volumeChange', (volume, treshold) => {
    showVolume(document.getElementById('localVolume'), volume);
});
// remote volume has changed
webRTC.on('remoteVolumeChange', (peer, volume) => {
    showVolume(document.getElementById('volume_' + peer.id), volume);
});

// local p2p/ice failure
webRTC.on('iceFailed', (peer) => {
    const connstate = document.querySelector('#container_' + webRTC.getDomId(peer) + ' .connectionstate');
    console.log('local fail', connstate);
    if (connstate) {
        connstate.innerText = 'Connection failed.';
        fileinput.disabled = 'disabled';
    }
});

// remote p2p/ice failure
webRTC.on('connectivityError', (peer) => {
    const connstate = document.querySelector('#container_' + webRTC.getDomId(peer) + ' .connectionstate');
    console.log('remote fail', connstate);
    if (connstate) {
        connstate.innerText = 'Connection failed.';
        fileinput.disabled = 'disabled';
    }
});

// Since we use this twice we put it here
const setRoom = (name) => {
    document.querySelector('form').remove();
    document.getElementById('title').innerText = 'Room: ' + name;
    document.getElementById('subTitle').innerHTML =  `Link to join: <a href="${window.location.href}" target="_blank">${window.location.href}</a>`;
    $('body').addClass('active');
}

if (room) {
    setRoom(room);
} else {
    $('form').submit(() => {
        const val = $('#sessionInput').val().toLowerCase().replace(/\s/g, '-').replace(/[^A-Za-z0-9_\-]/g, '');
        webRTC.createRoom(val, (err, name) => {
            console.log(' create room ' + name);

            const newUrl = window.location.pathname + '?' + name;
            if (!err) {
                window.history.replaceState({foo: 'bar'}, null, newUrl);
                setRoom(name);
            } else {
                console.log(err);
            }
        });
        return false;
    });
}

/* const button = document.getElementById('screenShareButton')
const setButton = (bool) => {
    button.innerText = bool ? 'share screen' : 'stop sharing';
};
if (!webRTC.capabilities.supportScreenSharing) {
    button.disabled = 'disabled';
}
webRTC.on('localScreenRemoved', () => {
    setButton(true);
});

setButton(true);

button.onclick = () => {
    if (webRTC.getLocalScreen()) {
        webRTC.stopScreenShare();
        setButton(true);
    } else {
        webRTC.shareScreen((err) => {
            if (err) {
                setButton(true);
            } else {
                setButton(false);
            }
        });

    }
}; */