'use strict';

// Build the connection url
const getConnectionUrl = (user, password, host, port, dbName) => {
    return `mongodb://${user}:${password}@${host}:${port}/${dbName}`;
};

class Database {
    constructor() {
        this.url = getConnectionUrl(
            process.env.DB_USER,
            process.env.DB_PASS,
            process.env.DB_HOST,
            process.env.DB_PORT,
            'surveillance'
        );
        this.mongoose = require('mongoose');
        this.Schema = this.mongoose.Schema;
    }

    connect() {
        return this.mongoose.connect(this.url);
    }

    getModel(schema, name) {
        const s = new this.Schema(schema);
        return this.mongoose.model(name, s);
    }

    getSchema() {
        return this.mongoose.Schema;
    }
}

module.exports = new Database();
