const DB = require('./database');

module.exports = {
    Surveillance: DB.getModel({
        time: Date,
        category: String,
        title: String,
        details: String,
        coordinates: {
            lat: Number,
            lng: Number,
        },
        thumbnail: String,
        image: String,
        original: String,
    }, 'Surveillance'),
};
